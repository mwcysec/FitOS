package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.R;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.ll_food) LinearLayout ivFood;
    @BindView(R.id.ll_exercise) LinearLayout ivExercise;
    @BindView(R.id.ll_run) LinearLayout ivRun;
    @BindView(R.id.ll_heart) LinearLayout ivHeart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }


    @OnClick({R.id.ll_food, R.id.ll_exercise, R.id.ll_run, R.id.ll_heart})
    public void openActivity(LinearLayout item) {
        Intent action;
        switch(item.getId()) {
            case R.id.ll_food:
                action = new Intent (this, FoodActivity.class);
                break;
            case R.id.ll_exercise:
                action = new Intent (this, ExerciseActivity.class);
                break;
            case R.id.ll_run:
                action = new Intent (this, RunActivity.class);
                break;
            default:
                action = new Intent(this, HRMActivity.class);
                break;
        }
        startActivity(action);
    }


}
