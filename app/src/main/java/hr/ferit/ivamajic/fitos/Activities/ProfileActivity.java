package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.R;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.tvProfileLogout) TextView logoutTv;
    @BindView(R.id.tvProfileAbout) TextView tvAbout;
    @BindView(R.id.tvProfileUsername) TextView tvUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ButterKnife.bind(this);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid());
        database.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tvUsername.setText(dataSnapshot.child("username").getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.tvProfileLogout, R.id.tvProfileAbout})
    public void manageClick(TextView textView) {
        Intent action = new Intent();
        switch(textView.getId()) {
            case R.id.tvProfileLogout:
                FirebaseAuth.getInstance().signOut();
                action = new Intent(this, LoginActivity.class);
                finish();
                break;
            case R.id.tvProfileAbout:
                action = new Intent(this, AboutActivity.class);
                break;
        }
        startActivity(action);
    }
}
