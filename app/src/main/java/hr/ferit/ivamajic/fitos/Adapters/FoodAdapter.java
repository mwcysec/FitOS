package hr.ferit.ivamajic.fitos.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import hr.ferit.ivamajic.fitos.Models.Food;
import hr.ferit.ivamajic.fitos.Models.FoodEntry;
import hr.ferit.ivamajic.fitos.R;

public class FoodAdapter extends BaseAdapter {

    private ArrayList<FoodEntry> mFoods;

    public FoodAdapter(ArrayList<FoodEntry> foods) {
        mFoods = foods;
    }

    @Override
    public int getCount() {
        return this.mFoods.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mFoods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder foodViewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_food, parent, false);
            foodViewHolder = new ViewHolder(convertView);
            convertView.setTag(foodViewHolder);
        }
        else {
            foodViewHolder = (ViewHolder) convertView.getTag();
        }

        FoodEntry food = this.mFoods.get(position);
        foodViewHolder.tvFoodName.setText(food.getFood().getName());
        foodViewHolder.tvFoodQuantity.setText(String.valueOf(food.getQuantity()));
        foodViewHolder.tvFoodUnit.setText(food.getUnit());
        foodViewHolder.tvFoodKcal.setText(String.valueOf(food.getFood().getCalories()));
        foodViewHolder.tvFoodProtein.setText(String.valueOf(food.getFood().getProteins()));
        foodViewHolder.tvFoodFat.setText(String.valueOf(food.getFood().getFats()));
        foodViewHolder.tvFoodCarb.setText(String.valueOf(food.getFood().getCarbohydrates()));

        return convertView;
    }

    public static class ViewHolder {
        public TextView tvFoodName, tvFoodQuantity, tvFoodUnit, tvFoodKcal, tvFoodProtein, tvFoodFat, tvFoodCarb;

        public ViewHolder(View foodView) {
            tvFoodName = (TextView) foodView.findViewById(R.id.tvFoodItemName);
            tvFoodQuantity = (TextView) foodView.findViewById(R.id.tvFoodItemQuantity);
            tvFoodUnit = (TextView) foodView.findViewById(R.id.tvFoodItemUnit);
            tvFoodKcal = (TextView) foodView.findViewById(R.id.tvFoodItemKcal);
            tvFoodProtein = (TextView) foodView.findViewById(R.id.tvFoodItemProtein);
            tvFoodFat = (TextView) foodView.findViewById(R.id.tvFoodItemFat);
            tvFoodCarb = (TextView) foodView.findViewById(R.id.tvFoodItemCarb);
        }
    }
}
