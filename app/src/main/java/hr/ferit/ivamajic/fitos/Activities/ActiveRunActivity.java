package hr.ferit.ivamajic.fitos.Activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.RunEntry;
import hr.ferit.ivamajic.fitos.Models.SimpleLocationListener;
import hr.ferit.ivamajic.fitos.R;

public class ActiveRunActivity extends AppCompatActivity {

    private static final int REQUEST_LOCATION_PERMISSION = 10;
    @BindView(R.id.btnActiveRunFinish)
    Button btnFinish;
    @BindView(R.id.btnActiveRunPause)
    Button btnPause;
    @BindView(R.id.tvActiveRunSpeed)
    TextView tvSpeed;
    private Chronometer chronometer;
    private Boolean paused = false;
    private long timeWhenStopped;

    private double maxSpeed = 0;

    LocationManager locationManager;
    LocationListener locationListener;

    private long lastUpdate;

    private double distance = 0;

    private double lastLongitude;
    private double lastLatitude;

    private ArrayList<hr.ferit.ivamajic.fitos.Models.Location> route = new ArrayList<>();

    private boolean firstUpdate = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_run);

        ButterKnife.bind(this);

        tvSpeed.setText("0.00");

        chronometer = (Chronometer) findViewById(R.id.chronometerActiveRun);
        chronometer.setBase(SystemClock.elapsedRealtime());
        chronometer.start();


        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                updateLocationDisplay(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (hasLocationPermission() == false) {
            requestPermission();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTracking();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this.hasLocationPermission()) {
            startTracking();
        }
    }



    private void startTracking() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        String locationProvider = this.locationManager.getBestProvider(criteria, true);
        long minTime = 1000;
        long minDistance = 10;
        this.locationManager.requestLocationUpdates(locationProvider, minTime, minDistance, this.locationListener);
    }

    private void stopTracking() {
        this.locationManager.removeUpdates(this.locationListener);
    }

    private boolean hasLocationPermission() {
        String LocationPermission = Manifest.permission.ACCESS_FINE_LOCATION;
        int status = ContextCompat.checkSelfPermission(this, LocationPermission);
        if (status == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }

    private void requestPermission() {
        String[] permissions = new String[] {
                Manifest.permission.ACCESS_FINE_LOCATION
        };
        ActivityCompat.requestPermissions(ActiveRunActivity.this, permissions, REQUEST_LOCATION_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case REQUEST_LOCATION_PERMISSION:
                if (grantResults.length>0) {
                    if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                        askForPermission();
                    }
                }
        }
    }

    private void askForPermission() {
        boolean shouldExplain = ActivityCompat.shouldShowRequestPermissionRationale(ActiveRunActivity.this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (shouldExplain) {
            this.displayDialog();
        }
        else {
            finish();
        }
    }

    private void displayDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Location permission")
                .setMessage("Location permission needed")
                .setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        requestPermission();
                        dialogInterface.cancel();
                    }
                })
                .show();
    }

    @OnClick({R.id.btnActiveRunPause})
    public void pauseRun() {
        if (!paused) {
            chronometer.stop();
            timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
            paused = true;
            btnPause.setText(R.string.resume);
        }
        else {
            chronometer.setBase(SystemClock.elapsedRealtime() + timeWhenStopped);
            chronometer.start();
            paused = false;
            btnPause.setText(R.string.pause);
        }

    }

    @OnClick({R.id.btnActiveRunFinish})
    public void finishRun() {
        paused = true;
        chronometer.stop();
        timeWhenStopped = chronometer.getBase() - SystemClock.elapsedRealtime();
        double duration = (SystemClock.elapsedRealtime() - chronometer.getBase())/1000.00/60;
        this.locationManager.removeUpdates(this.locationListener);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        String date = sdf.format(today);
        Double avgSpeed = distance / (duration / 60);

        RunEntry runEntry = new RunEntry(null, date, duration, distance, avgSpeed, maxSpeed * 3.6, route);
        Intent action = new Intent(this, FinishRunActivity.class);
        action.putExtra("runEntry", new Gson().toJson(runEntry));
        startActivity(action);
        finish();
    }

    public void updateLocationDisplay(Location location) {
        double latitude = (double) (location.getLatitude());
        double longitude = (double) (location.getLongitude());
        float[] distances = new float[1];
        Location.distanceBetween(lastLatitude, lastLongitude, latitude, longitude, distances);
        if (!firstUpdate) {
            distance += distances[0]/1000;
        }
        lastLatitude = latitude;
        lastLongitude = longitude;

        double speed = location.getSpeed();
        Log.d("test23", String.valueOf(speed));
        tvSpeed.setText(String.format("%.2f", speed * 3.6) + " km/h");
        if (speed > maxSpeed) {
            maxSpeed = speed;
        }

        hr.ferit.ivamajic.fitos.Models.Location loc = new hr.ferit.ivamajic.fitos.Models.Location(latitude, longitude);
        route.add(loc);
        firstUpdate = false;
    }
}