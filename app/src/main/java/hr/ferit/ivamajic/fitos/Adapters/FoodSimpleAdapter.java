package hr.ferit.ivamajic.fitos.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hr.ferit.ivamajic.fitos.Models.API.FoodSearch.MyItem;
import hr.ferit.ivamajic.fitos.R;

public class FoodSimpleAdapter extends BaseAdapter {

    private ArrayList<MyItem> mFoods;

    public FoodSimpleAdapter(ArrayList<MyItem> foods) {
        mFoods = foods;
    }

    @Override
    public int getCount() {
        return this.mFoods.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mFoods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder foodViewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_food_simple, parent, false);
            foodViewHolder = new ViewHolder(convertView);
            convertView.setTag(foodViewHolder);
        }
        else {
            foodViewHolder = (ViewHolder) convertView.getTag();
        }

        MyItem food = this.mFoods.get(position);
        foodViewHolder.tvFoodName.setText(food.getName());
        return convertView;
    }

    public static class ViewHolder {
        public TextView tvFoodName;

        public ViewHolder(View foodView) {
            tvFoodName = (TextView) foodView.findViewById(R.id.tvFoodItemSimpleName);
        }
    }
}
