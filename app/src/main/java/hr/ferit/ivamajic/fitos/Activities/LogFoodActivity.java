package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Adapters.NutrientAdapter;
import hr.ferit.ivamajic.fitos.Models.API.FoodDetails.FoodDetailsResponse;
import hr.ferit.ivamajic.fitos.Models.API.FoodDetails.Nutrient;
import hr.ferit.ivamajic.fitos.Models.Food;
import hr.ferit.ivamajic.fitos.Models.FoodEntry;
import hr.ferit.ivamajic.fitos.R;

public class LogFoodActivity extends AppCompatActivity {

    @BindView(R.id.spinnerLogFoodCategory) Spinner spinnerCategory;
    @BindView(R.id.tvLogFoodUnit) TextView tvUnit;
    @BindView(R.id.etLogFoodQuantity) EditText etQuantity;
    @BindView(R.id.tvLogFoodName) TextView tvName;
    @BindView(R.id.btnLogFoodLog) Button btnLog;

    ListView lvNutrients;
    NutrientAdapter nutrientAdapter;

    FoodDetailsResponse chosenFoodDetails;
    FoodDetailsResponse chosenFoodDetailsEditable;

    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_food);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();

        date = bundle != null ? bundle.getString("date") : null;
        String chosenFood = bundle != null ? bundle.getString("details") : null;
        chosenFoodDetails = new Gson().fromJson(chosenFood, FoodDetailsResponse.class);
        chosenFoodDetailsEditable = new Gson().fromJson(chosenFood, FoodDetailsResponse.class);

        tvName.setText(chosenFoodDetailsEditable.getFoods().get(0).getFood().getDesc().getName());
        etQuantity.setText("100");
        tvUnit.setText(chosenFoodDetailsEditable.getFoods().get(0).getFood().getDesc().getRu());

        lvNutrients = (ListView) findViewById(R.id.lvLogFoodNutrients);
        nutrientAdapter = new NutrientAdapter(chosenFoodDetailsEditable.getFoods().get(0).getFood().getNutrients());
        lvNutrients.setAdapter(nutrientAdapter);


        /* Categories spinner initialization */
        ArrayList<String> categories = new ArrayList<>();
        categories.add("Breakfast");
        categories.add("Lunch");
        categories.add("Snacks");
        categories.add("Dinner");
        ArrayAdapter<String> spinnerAdapterCategories = new ArrayAdapter<>(this, R.layout.spinner_layout, categories);
        spinnerAdapterCategories.setDropDownViewResource(R.layout.spinner_layout);
        spinnerCategory.setAdapter(spinnerAdapterCategories);

        etQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                double coef;
                if(editable.toString().equals("")) {
                    coef = 0;
                }
                else {
                    coef = Double.parseDouble(editable.toString()) / 100.00;
                }
                int i = 0;
                for (Nutrient nutrient: chosenFoodDetailsEditable.getFoods().get(0).getFood().getNutrients()) {
                    DecimalFormat df = new DecimalFormat("#.#");
                    double startValue = Double.parseDouble(chosenFoodDetails.getFoods().get(0).getFood().getNutrients().get(i).getValue());
                    nutrient.setValue(String.valueOf(df.format(startValue * coef)));
                    i++;
                }
                lvNutrients.setAdapter(nutrientAdapter);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnLogFoodLog})
    public void logFood() {
        Food food = new Food(null, tvName.getText().toString());
        for (Nutrient nutrient: chosenFoodDetailsEditable.getFoods().get(0).getFood().getNutrients()) {
            switch (nutrient.getName()) {
                case "Energy":
                    food.setCalories(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Protein":
                    food.setProteins(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Total lipid (fat)":
                    food.setFats(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Carbohydrate, by difference":
                    food.setCarbohydrates(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Fiber, total dietary":
                    food.setFibers(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Fatty acids, total saturated":
                    food.setSaturatedFats(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Fatty acids, total monounsaturated":
                    food.setMonoUnsaturatedFats(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Fatty acids, total polyunsaturated":
                    food.setPolyUnsaturatedFats(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Sugars, total":
                    food.setSugars(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Fatty acids, total trans":
                    food.setTransFats(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Iron, Fe":
                    food.setIron(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Sodium, Na":
                    food.setSodium(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Calcium, Ca":
                    food.setCalcium(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Cholesterol":
                    food.setCholesterol(Double.parseDouble(nutrient.getValue()));
                    break;
                case "Potassium, K":
                    food.setPotassium(Double.parseDouble(nutrient.getValue()));
                    break;
                default:
                    break;
            }
        }


        String unit = tvUnit.getText().toString();
        String category = spinnerCategory.getSelectedItem().toString();
        Double quantity = Double.parseDouble(etQuantity.getText().toString());

        FoodEntry foodEntry = new FoodEntry(null, unit, category, date, food, quantity);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference database = FirebaseDatabase.getInstance().getReference("foodEntries/" + user.getUid());
        database.push().setValue(foodEntry);
        Toast.makeText(this, "Successfully added food", Toast.LENGTH_SHORT).show();
        finish();
    }
}
