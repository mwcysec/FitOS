package hr.ferit.ivamajic.fitos.Models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import hr.ferit.ivamajic.fitos.R;

public class ExerciseEntry {

    private String id, date;
    private Exercise exercise;

    public ExerciseEntry(String id, String date, Exercise exercise) {
        this.id = id;
        this.date = date;
        this.exercise = exercise;
    }

    public Exercise getExercise() {
        return exercise;
    }

    public void setExercise(Exercise exercise) {
        this.exercise = exercise;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
