package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.User;
import hr.ferit.ivamajic.fitos.R;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.etSignupEmail) EditText etEmail;
    @BindView(R.id.etSignupFirstName) EditText etFirstName;
    @BindView(R.id.etSignupLastName) EditText etLastName;
    @BindView(R.id.etSignupUsername) EditText etUsername;
    @BindView(R.id.etSignupPassword) EditText etPassword;
    @BindView(R.id.etSignupConfirmPassword) EditText etConfirmPassword;

    @BindView(R.id.btnSignupSignUp) Button btnSignUp;

    private FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        auth = FirebaseAuth.getInstance();

        ButterKnife.bind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }

    @OnClick({R.id.btnSignupSignUp})
    public void signUp() {
        if (etEmail.getText().toString().equals("") || etFirstName.getText().toString().equals("") || etLastName.getText().toString().equals("") || etUsername.getText().toString().equals("") || etPassword.getText().toString().equals("") || etConfirmPassword.getText().toString().equals("")) {
            Toast.makeText(this, "Fill out all the fields!", Toast.LENGTH_SHORT).show();
        }
        else {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("users");
            Query usernameQuery = database.orderByChild("username").equalTo(etUsername.getText().toString());
            usernameQuery.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getChildrenCount() > 0) {
                        Toast.makeText(SignupActivity.this, "Username already exists!", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (!etPassword.getText().toString().equals(etConfirmPassword.getText().toString())) {
                            Toast.makeText(SignupActivity.this, "Passwords do not match!", Toast.LENGTH_SHORT).show();
                        }
                        else if (etPassword.getText().toString().length() < 6) {
                            Toast.makeText(SignupActivity.this, "Password must be at least 6 characters!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            final User newUser = new User(null, etEmail.getText().toString(), etFirstName.getText().toString(), etLastName.getText().toString(), etUsername.getText().toString());
                            auth.createUserWithEmailAndPassword(newUser.getEmail(), etPassword.getText().toString())
                                    .addOnCompleteListener(SignupActivity.this, new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful()) {
                                                FirebaseUser user = auth.getCurrentUser();
                                                DatabaseReference database = FirebaseDatabase.getInstance().getReference("users");
                                                database.child(user.getUid()).setValue(newUser);
                                                startActivity(new Intent(SignupActivity.this, AddWeightHeightActivity.class));
                                                finish();
                                            }
                                            else {
                                                Log.w("test2", "createUserWithEmail:failure", task.getException());
                                                Toast.makeText(SignupActivity.this, "Authentication failed: " + task.getException(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

        }
    }
}
