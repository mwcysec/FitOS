package hr.ferit.ivamajic.fitos.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import hr.ferit.ivamajic.fitos.Models.Exercise;
import hr.ferit.ivamajic.fitos.Models.ExerciseEntry;
import hr.ferit.ivamajic.fitos.Models.Food;
import hr.ferit.ivamajic.fitos.Models.FoodEntry;
import hr.ferit.ivamajic.fitos.Models.RunEntry;
import hr.ferit.ivamajic.fitos.R;

public class RunAdapter extends BaseAdapter {

    private ArrayList<RunEntry> mRuns;

    public RunAdapter(ArrayList<RunEntry> runs) {
        mRuns = runs;
    }

    @Override
    public int getCount() {
        return this.mRuns.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mRuns.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder runViewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_run, parent, false);
            runViewHolder = new ViewHolder(convertView);
            convertView.setTag(runViewHolder);
        }
        else {
            runViewHolder = (ViewHolder) convertView.getTag();
        }

        RunEntry run = this.mRuns.get(position);

        runViewHolder.tvRunDate.setText(String.valueOf(run.getDate()));

        return convertView;
    }

    public static class ViewHolder {
        public TextView tvRunDate;

        public ViewHolder(View exerciseView) {

            tvRunDate = (TextView) exerciseView.findViewById(R.id.tvItemRunDate);

        }
    }
}
