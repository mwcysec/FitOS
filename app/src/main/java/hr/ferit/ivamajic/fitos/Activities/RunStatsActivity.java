package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.ivamajic.fitos.Models.RunEntry;
import hr.ferit.ivamajic.fitos.R;

public class RunStatsActivity extends AppCompatActivity {

    @BindView(R.id.tvRunStatsTitle) TextView  tvTitle;
    @BindView(R.id.tvRunStatsAvgSpeed) TextView tvAvgSpeed;
    @BindView(R.id.tvRunStatsMaxSpeed) TextView tvMaxSpeed;
    @BindView(R.id.tvRunStatsDistance) TextView tvDistance;
    @BindView(R.id.tvRunStatsDuration) TextView tvDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_stats);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();

        String chosenRun = bundle != null ? bundle.getString("run") : null;
        RunEntry chosenRunEntry = new Gson().fromJson(chosenRun, RunEntry.class);

        tvTitle.setText(chosenRunEntry.getDate() + " run details");
        tvAvgSpeed.setText(String.format("%.2f", chosenRunEntry.getAvgSpeed()));
        tvMaxSpeed.setText(String.format("%.2f", chosenRunEntry.getMaxSpeed()));
        tvDistance.setText(String.format("%.2f", chosenRunEntry.getDistance()));
        tvDuration.setText(String.format("%.2f", chosenRunEntry.getDuration()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }
}
