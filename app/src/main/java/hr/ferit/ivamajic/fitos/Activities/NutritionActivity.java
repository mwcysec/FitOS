package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.ivamajic.fitos.Models.FoodEntry;
import hr.ferit.ivamajic.fitos.R;

public class NutritionActivity extends AppCompatActivity {

    @BindView(R.id.tvNutritionCalcium) TextView tvCalcium;
    @BindView(R.id.tvNutritionCarbohydrates) TextView tvCarbohydrates;
    @BindView(R.id.tvNutritionCholesterol) TextView tvCholesterol;
    @BindView(R.id.tvNutritionKcal) TextView tvKcal;
    @BindView(R.id.tvNutritionProtein) TextView tvProtein;
    @BindView(R.id.tvNutritionFats) TextView tvFats;
    @BindView(R.id.tvNutritionFibers) TextView tvFibers;
    @BindView(R.id.tvNutritionIron) TextView tvIron;
    @BindView(R.id.tvNutritionMonoUnsaturatedFats) TextView tvMonoUnsaturatedFats;
    @BindView(R.id.tvNutritionPolyunsaturatedFats) TextView tvPolyunsaturatedFats;
    @BindView(R.id.tvNutritionPotassium) TextView tvPotassium;
    @BindView(R.id.tvNutritionSaturatedFats) TextView tvSaturatedFats;
    @BindView(R.id.tvNutritionSodium) TextView tvSodium;
    @BindView(R.id.tvNutritionSugars) TextView tvSugars;
    @BindView(R.id.tvNutritionTransFats) TextView tvTransFats;
    @BindView(R.id.tvNutritionGoalCarbohydrates) TextView tvGoalCarbs;
    @BindView(R.id.tvNutritionGoalKcal) TextView tvGoalKcal;
    @BindView(R.id.tvNutritionGoalProtein) TextView tvGoalProtein;
    @BindView(R.id.tvNutritionGoalFats) TextView tvGoalFats;
    @BindView(R.id.tvNutritionDate) TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nutrition);

        Bundle bundle = this.getIntent().getExtras();

        ButterKnife.bind(this);

        String calories = bundle != null ? bundle.getString("calories") : null;
        String protein = bundle != null ? bundle.getString("proteins") : null;
        String carbohydrates = bundle != null ? bundle.getString("carbohydrates") : null;
        String fats = bundle != null ? bundle.getString("fats") : null;
        String calcium = bundle != null ? bundle.getString("calcium") : null;
        String cholesterol = bundle != null ? bundle.getString("cholesterol") : null;
        String fibers = bundle != null ? bundle.getString("fibers") : null;
        String iron = bundle != null ? bundle.getString("iron") : null;
        String monoFats = bundle != null ? bundle.getString("monoUnsaturatedFats") : null;
        String polyFats = bundle != null ? bundle.getString("polyunsaturatedFats") : null;
        String potassium = bundle != null ? bundle.getString("potassium") : null;
        String satFats = bundle != null ? bundle.getString("saturatedFats") : null;
        String sodium = bundle != null ? bundle.getString("sodium") : null;
        String sugars = bundle != null ? bundle.getString("sugars") : null;
        String transFats = bundle != null ? bundle.getString("transFats") : null;

        String date = bundle != null ? bundle.getString("date") : null;

        tvKcal.setText(calories);
        tvProtein.setText(protein);
        tvCarbohydrates.setText(carbohydrates);
        tvFats.setText(fats);
        tvCalcium.setText(calcium);
        tvCholesterol.setText(cholesterol);
        tvFibers.setText(fibers);
        tvIron.setText(iron);
        tvMonoUnsaturatedFats.setText(monoFats);
        tvPolyunsaturatedFats.setText(polyFats);
        tvPotassium.setText(potassium);
        tvSaturatedFats.setText(satFats);
        tvSodium.setText(sodium);
        tvSugars.setText(sugars);
        tvTransFats.setText(transFats);

        tvDate.setText(date);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user==null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("users/" + user.getUid());
            database.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String kcalGoal = dataSnapshot.child("calorieGoal").getValue().toString();
                    String proteinGoal = dataSnapshot.child("proteinGoal").getValue().toString();
                    String carbGoal = dataSnapshot.child("carbGoal").getValue().toString();
                    String fatGoal = dataSnapshot.child("fatGoal").getValue().toString();

                    tvGoalKcal.setText(kcalGoal);
                    tvGoalProtein.setText(proteinGoal);
                    tvGoalCarbs.setText(carbGoal);
                    tvGoalFats.setText(fatGoal);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }
}
