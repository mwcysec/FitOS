package hr.ferit.ivamajic.fitos.Models.API.FoodDetails;

public class Footnote {
    private String idv, desc;

    public String getDesc() {
        return desc;
    }

    public String getIdv() {
        return idv;
    }
}
