package hr.ferit.ivamajic.fitos.Models.API.FoodDetails;

import java.util.ArrayList;

public class FoodDetails {
    private String sr, type;
    private Desc desc;
    private Ing ing;
    private ArrayList<Nutrient> nutrients;
    private ArrayList<Footnote> footnotes;

    public String getSr() {
        return sr;
    }

    public String getType() {
        return type;
    }

    public Desc getDesc() {
        return desc;
    }

    public Ing getIng() {
        return ing;
    }

    public ArrayList<Nutrient> getNutrients() {
        return nutrients;
    }

    public ArrayList<Footnote> getFootnotes() {
        return footnotes;
    }
}
