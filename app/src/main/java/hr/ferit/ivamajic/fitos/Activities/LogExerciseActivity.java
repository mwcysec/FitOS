package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Models.Exercise;
import hr.ferit.ivamajic.fitos.Models.ExerciseEntry;
import hr.ferit.ivamajic.fitos.R;

public class LogExerciseActivity extends AppCompatActivity {

    @BindView(R.id.etLogExerciseName) EditText etName;
    @BindView(R.id.etLogExerciseSeries) EditText etSeries;
    @BindView(R.id.etLogExerciseRepetitions) EditText etRepetitions;
    @BindView(R.id.etLogExerciseWeight) EditText etWeight;
    @BindView(R.id.btnLogExerciseLog) Button btnLog;

    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_exercise);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();

        date = bundle != null ? bundle.getString("date") : null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnLogExerciseLog})
    public void logExercise() {
        if(etName.getText().toString().equals("") || etSeries.getText().toString().equals("") || etRepetitions.getText().toString().equals("") || etWeight.getText().toString().equals("")) {
            Toast.makeText(this, "Fill out all the fields!", Toast.LENGTH_SHORT).show();
        }
        else {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            if (user==null) {
                startActivity(new Intent(this, MainActivity.class));
            }
            else {
                Exercise exercise = new Exercise(etName.getText().toString(), Integer.parseInt(etRepetitions.getText().toString()), Integer.parseInt(etWeight.getText().toString()), Integer.parseInt(etSeries.getText().toString()));


                ExerciseEntry exerciseEntry = new ExerciseEntry(null, date, exercise);

                DatabaseReference database = FirebaseDatabase.getInstance().getReference("exerciseEntries/" + user.getUid());
                database.push().setValue(exerciseEntry);
                Toast.makeText(this, "Successfully added exercise", Toast.LENGTH_SHORT).show();
                finish();

            }
        }

    }
}
