package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Adapters.FoodSimpleAdapter;
import hr.ferit.ivamajic.fitos.Async.GetFoodDetails;
import hr.ferit.ivamajic.fitos.Async.GetSearchResults;
import hr.ferit.ivamajic.fitos.Models.API.FoodDetails.FoodDetailsResponse;
import hr.ferit.ivamajic.fitos.Models.API.FoodSearch.MyItem;
import hr.ferit.ivamajic.fitos.R;

public class SearchFoodActivity extends AppCompatActivity {

    private ListView lvResults;

    private GetSearchResults getSearchResultsTask;
    private GetFoodDetails getFoodDetailsTask;

    @BindView(R.id.etSearchFoodName) EditText etFoodName;
    @BindView(R.id.btnSearchFoodSearch) Button btnSearch;

    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_food);

        ButterKnife.bind(this);



        Bundle bundle = this.getIntent().getExtras();

        date = bundle != null ? bundle.getString("date") : null;


        lvResults = (ListView) findViewById(R.id.lvSearchFoodResults);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(this.getSearchResultsTask != null) {
            this.getSearchResultsTask.detach();
        }
    }

    @OnClick({R.id.btnSearchFoodSearch})
    public void findFoods() {
        final String searchTerm = etFoodName.getText().toString();
        this.getSearchResultsTask = new GetSearchResults(this);
        this.getSearchResultsTask.execute(searchTerm);



    }

    public void displayResult(final ArrayList<MyItem> items) {
        FoodSimpleAdapter foodAdapter = new FoodSimpleAdapter(items);
        final SearchFoodActivity that = this;
        lvResults.setAdapter(foodAdapter);
        lvResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MyItem chosenItem = items.get(i);
                getFoodDetailsTask = new GetFoodDetails(that);
                getFoodDetailsTask.execute(chosenItem.getNdbno());
            }
        });
    }

    public void displayFoodDetails(FoodDetailsResponse details) {
        Intent action = new Intent(this, LogFoodActivity.class);
        action.putExtra("details", new Gson().toJson(details));
        action.putExtra("date", date);
        startActivity(action);
    }
}
