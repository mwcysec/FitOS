package hr.ferit.ivamajic.fitos.Models;

import java.util.ArrayList;

public class FoodEntry {

    private String id, unit, category, date;
    private Food food;
    private double quantity;


    public FoodEntry(String id, String unit, String category, String date, Food food, double quantity) {
        this.id = id;
        this.unit = unit;
        this.category = category;
        this.date = date;
        this.food = food;
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
