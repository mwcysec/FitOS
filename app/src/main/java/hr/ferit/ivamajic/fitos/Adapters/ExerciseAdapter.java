package hr.ferit.ivamajic.fitos.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

import hr.ferit.ivamajic.fitos.Models.Exercise;
import hr.ferit.ivamajic.fitos.Models.ExerciseEntry;
import hr.ferit.ivamajic.fitos.Models.Food;
import hr.ferit.ivamajic.fitos.Models.FoodEntry;
import hr.ferit.ivamajic.fitos.R;

public class ExerciseAdapter extends BaseAdapter {

    private ArrayList<ExerciseEntry> mExercises;

    public ExerciseAdapter(ArrayList<ExerciseEntry> exercises) {
        mExercises = exercises;
    }

    @Override
    public int getCount() {
        return this.mExercises.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mExercises.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder exerciseViewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_exercise, parent, false);
            exerciseViewHolder = new ViewHolder(convertView);
            convertView.setTag(exerciseViewHolder);
        }
        else {
            exerciseViewHolder = (ViewHolder) convertView.getTag();
        }

        ExerciseEntry exercise = this.mExercises.get(position);

        exerciseViewHolder.tvExerciseWeight.setText(String.valueOf(exercise.getExercise().getWeight()));
        exerciseViewHolder.tvExerciseRepetitions.setText(String.valueOf(exercise.getExercise().getRepetitions()));
        exerciseViewHolder.tvExerciseSeries.setText(String.valueOf(exercise.getExercise().getSeriesNo()));
        exerciseViewHolder.tvExerciseName.setText(exercise.getExercise().getName());

        return convertView;
    }

    public static class ViewHolder {
        public TextView tvExerciseName, tvExerciseSeries, tvExerciseRepetitions, tvExerciseWeight;

        public ViewHolder(View exerciseView) {

            tvExerciseName = (TextView) exerciseView.findViewById(R.id.tvItemExerciseName);
            tvExerciseSeries = (TextView) exerciseView.findViewById(R.id.tvItemExerciseSeries);
            tvExerciseRepetitions = (TextView) exerciseView.findViewById(R.id.tvItemExerciseRepetitions);
            tvExerciseWeight = (TextView) exerciseView.findViewById(R.id.tvItemExerciseWeight);

        }
    }
}
