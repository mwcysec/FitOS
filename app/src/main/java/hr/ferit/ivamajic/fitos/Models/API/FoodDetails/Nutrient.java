package hr.ferit.ivamajic.fitos.Models.API.FoodDetails;

import java.util.ArrayList;

public class Nutrient {
    private String nutrient_id, name, derivation, group, unit, value;
    private ArrayList<MyMeasure> measures;

    public ArrayList<MyMeasure> getMeasures() {
        return measures;
    }

    public String getNutrient_id() {
        return nutrient_id;
    }

    public String getName() {
        return name;
    }

    public String getDerivation() {
        return derivation;
    }

    public String getGroup() {
        return group;
    }

    public String getUnit() {
        return unit;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
