package hr.ferit.ivamajic.fitos.Models.API.FoodDetails;

public class Desc {
    private String ndbno, name, ds, manu, ru;

    public String getRu() {
        return ru;
    }

    public String getManu() {
        return manu;
    }

    public String getDs() {
        return ds;
    }

    public String getName() {
        return name;
    }

    public String getNdbno() {
        return ndbno;
    }
}
