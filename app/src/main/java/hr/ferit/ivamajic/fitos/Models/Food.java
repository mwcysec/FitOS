package hr.ferit.ivamajic.fitos.Models;

public class Food {
    private String name;
    private double carbohydrates, calories, proteins, fats, saturatedFats, polyUnsaturatedFats, monoUnsaturatedFats, transFats, sugars, fibers, iron, sodium, calcium, cholesterol, potassium;

    public Food(String id, String name) {
        this.name = name;
    }

    public Food(String name, double carbohydrates, double calories, double proteins, double fats, double saturatedFats, double polyUnsaturatedFats, double monoUnsaturatedFats, double transFats, double sugars, double fibers, double iron, double sodium, double calcium, double cholesterol, double potassium) {

        this.name = name;
        this.carbohydrates = carbohydrates;
        this.calories = calories;
        this.proteins = proteins;
        this.fats = fats;
        this.saturatedFats = saturatedFats;
        this.polyUnsaturatedFats = polyUnsaturatedFats;
        this.monoUnsaturatedFats = monoUnsaturatedFats;
        this.transFats = transFats;
        this.sugars = sugars;
        this.fibers = fibers;
        this.iron = iron;
        this.sodium = sodium;
        this.calcium = calcium;
        this.cholesterol = cholesterol;
        this.potassium = potassium;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCarbohydrates() {
        return carbohydrates;
    }

    public void setCarbohydrates(double carbohydrates) {
        this.carbohydrates = carbohydrates;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public double getFats() {
        return fats;
    }

    public void setFats(double fats) {
        this.fats = fats;
    }

    public double getSaturatedFats() {
        return saturatedFats;
    }

    public void setSaturatedFats(double saturatedFats) {
        this.saturatedFats = saturatedFats;
    }

    public double getMonoUnsaturatedFats() {
        return monoUnsaturatedFats;
    }

    public void setMonoUnsaturatedFats(double monoUnsaturatedFats) {
        this.monoUnsaturatedFats = monoUnsaturatedFats;
    }

    public double getTransFats() {
        return transFats;
    }

    public void setTransFats(double transFats) {
        this.transFats = transFats;
    }

    public double getSugars() {
        return sugars;
    }

    public void setSugars(double sugars) {
        this.sugars = sugars;
    }

    public double getFibers() {
        return fibers;
    }

    public void setFibers(double fibers) {
        this.fibers = fibers;
    }

    public double getIron() {
        return iron;
    }

    public void setIron(double iron) {
        this.iron = iron;
    }

    public double getSodium() {
        return sodium;
    }

    public void setSodium(double sodium) {
        this.sodium = sodium;
    }

    public double getCalcium() {
        return calcium;
    }

    public void setCalcium(double calcium) {
        this.calcium = calcium;
    }

    public double getPotassium() {
        return potassium;
    }

    public void setPotassium(double potassium) {
        this.potassium = potassium;
    }

    public double getCholesterol() {
        return cholesterol;
    }

    public void setCholesterol(double cholesterol) {
        this.cholesterol = cholesterol;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public double getPolyUnsaturatedFats() {
        return polyUnsaturatedFats;
    }

    public void setPolyUnsaturatedFats(double polyUnsaturatedFats) {
        this.polyUnsaturatedFats = polyUnsaturatedFats;
    }
}
