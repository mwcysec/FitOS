package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import hr.ferit.ivamajic.fitos.Adapters.FoodAdapter;
import hr.ferit.ivamajic.fitos.Models.API.FoodDetails.FoodDetailsResponse;
import hr.ferit.ivamajic.fitos.Models.Food;
import hr.ferit.ivamajic.fitos.Models.FoodEntry;
import hr.ferit.ivamajic.fitos.R;

public class FoodActivity extends AppCompatActivity {

    @BindView(R.id.btnFoodLogFood) Button btnLogFood;
    @BindView(R.id.tvFoodDate) TextView tvDate;
    @BindView(R.id.tvFoodPrevious) TextView tvPrevious;
    @BindView(R.id.tvFoodNext) TextView tvNext;
    @BindView(R.id.btnFoodNutrition) Button btnNutrition;
    ListView lvBreakfast, lvLunch, lvDinner, lvSnacks;

    Calendar currentDate, today, yesterday, tomorrow;

    SimpleDateFormat sdf;

    ArrayList<FoodEntry> foodEntries = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food);

        ButterKnife.bind(this);

        sdf = new SimpleDateFormat(getString(R.string.dateFormat));

        currentDate = Calendar.getInstance();
        tvDate.setText(R.string.today);


        today = Calendar.getInstance();
        int todayDate = today.get(Calendar.DAY_OF_MONTH);

        yesterday = Calendar.getInstance();
        yesterday.set(Calendar.DAY_OF_MONTH, todayDate - 1);

        tomorrow = Calendar.getInstance();
        tomorrow.set(Calendar.DAY_OF_MONTH, todayDate + 1);


        searchForFoods();
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }

    @OnClick({R.id.btnFoodLogFood})
    public void logFood() {
        Intent action = new Intent(this, SearchFoodActivity.class);
        action.putExtra("date", sdf.format(currentDate.getTime()));
        startActivity(action);
    }

    @OnClick({R.id.btnFoodNutrition})
    public void openNutrition() {
        double calcium = 0;
        double calories = 0;
        double carbohydrates = 0;
        double cholesterol = 0;
        double fats = 0;
        double fibers = 0;
        double iron = 0;
        double monoUnsaturatedFats = 0;
        double polyUnsaturatedFats = 0;
        double potassium = 0;
        double proteins = 0;
        double saturatedFats = 0;
        double sodium = 0;
        double sugars = 0;
        double transFats = 0;
        for (FoodEntry entry : foodEntries) {
            calcium += entry.getFood().getCalcium();
            calories += entry.getFood().getCalories();
            carbohydrates += entry.getFood().getCarbohydrates();
            cholesterol += entry.getFood().getCholesterol();
            fats += entry.getFood().getFats();
            fibers += entry.getFood().getFibers();
            iron += entry.getFood().getIron();
            monoUnsaturatedFats += entry.getFood().getMonoUnsaturatedFats();
            polyUnsaturatedFats += entry.getFood().getPolyUnsaturatedFats();
            potassium += entry.getFood().getPotassium();
            proteins += entry.getFood().getProteins();
            saturatedFats += entry.getFood().getSaturatedFats();
            sodium += entry.getFood().getSodium();
            sugars += entry.getFood().getSugars();
            transFats += entry.getFood().getTransFats();
        }

        Intent action = new Intent(this, NutritionActivity.class);
        action.putExtra("calcium", String.valueOf(calcium));
        action.putExtra("calories", String.valueOf(calories));
        action.putExtra("carbohydrates", String.valueOf(carbohydrates));
        action.putExtra("cholesterol", String.valueOf(cholesterol));
        action.putExtra("fats", String.valueOf(fats));
        action.putExtra("fibers", String.valueOf(fibers));
        action.putExtra("iron", String.valueOf(iron));
        action.putExtra("monoUnsaturatedFats", String.valueOf(monoUnsaturatedFats));
        action.putExtra("polyunsaturatedFats", String.valueOf(polyUnsaturatedFats));
        action.putExtra("potassium", String.valueOf(potassium));
        action.putExtra("proteins", String.valueOf(proteins));
        action.putExtra("saturatedFats", String.valueOf(saturatedFats));
        action.putExtra("sodium", String.valueOf(sodium));
        action.putExtra("sugars", String.valueOf(sugars));
        action.putExtra("transFats", String.valueOf(transFats));
        action.putExtra("date", sdf.format(currentDate.getTime()));
        startActivity(action);
    }


    public void searchForFoods() {
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("foodEntries/" + user.getUid());
            database.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    foodEntries = new ArrayList<>();
                    for (DataSnapshot foodEntrySnapshot : dataSnapshot.getChildren()) {
                        if (foodEntrySnapshot.child("date").getValue().toString().equals(sdf.format(currentDate.getTime()))) {
                            double calcium = Double.parseDouble(foodEntrySnapshot.child("food").child("calcium").getValue().toString());
                            double calories = Double.parseDouble(foodEntrySnapshot.child("food").child("calories").getValue().toString());
                            double carbohydrates = Double.parseDouble(foodEntrySnapshot.child("food").child("carbohydrates").getValue().toString());
                            double cholesterol = Double.parseDouble(foodEntrySnapshot.child("food").child("cholesterol").getValue().toString());
                            double fats = Double.parseDouble(foodEntrySnapshot.child("food").child("fats").getValue().toString());
                            double fibers = Double.parseDouble(foodEntrySnapshot.child("food").child("fibers").getValue().toString());
                            double iron = Double.parseDouble(foodEntrySnapshot.child("food").child("iron").getValue().toString());
                            double monoUnsaturatedFats = Double.parseDouble(foodEntrySnapshot.child("food").child("monoUnsaturatedFats").getValue().toString());
                            String name = foodEntrySnapshot.child("food").child("name").getValue().toString();
                            double polyUnsaturatedFats = Double.parseDouble(foodEntrySnapshot.child("food").child("polyUnsaturatedFats").getValue().toString());
                            double potassium = Double.parseDouble(foodEntrySnapshot.child("food").child("potassium").getValue().toString());
                            double proteins = Double.parseDouble(foodEntrySnapshot.child("food").child("proteins").getValue().toString());
                            double saturatedFats = Double.parseDouble(foodEntrySnapshot.child("food").child("saturatedFats").getValue().toString());
                            double sodium = Double.parseDouble(foodEntrySnapshot.child("food").child("sodium").getValue().toString());
                            double sugars = Double.parseDouble(foodEntrySnapshot.child("food").child("sugars").getValue().toString());
                            double transFats = Double.parseDouble(foodEntrySnapshot.child("food").child("transFats").getValue().toString());
                            Food food = new Food(name, carbohydrates, calories, proteins, fats, saturatedFats, polyUnsaturatedFats, monoUnsaturatedFats, transFats, sugars, fibers, iron, sodium, calcium, cholesterol, potassium);
                            FoodEntry foodEntry = new FoodEntry(foodEntrySnapshot.getKey(), foodEntrySnapshot.child("unit").getValue().toString(), foodEntrySnapshot.child("category").getValue().toString(), foodEntrySnapshot.child("date").getValue().toString(), food ,Double.parseDouble(foodEntrySnapshot.child("quantity").getValue().toString()));
                            foodEntries.add(foodEntry);
                        }
                    }
                    ArrayList<FoodEntry> breakfasts = new ArrayList<>();
                    ArrayList<FoodEntry> lunches = new ArrayList<>();
                    ArrayList<FoodEntry> dinners = new ArrayList<>();
                    ArrayList<FoodEntry> snacks = new ArrayList<>();
                    for (FoodEntry entry : foodEntries) {
                        switch (entry.getCategory()) {
                            case "Breakfast":
                                breakfasts.add(entry);
                                break;
                            case "Lunch":
                                lunches.add(entry);
                                break;
                            case "Dinner":
                                dinners.add(entry);
                                break;
                            default:
                                snacks.add(entry);
                                break;
                        }
                    }

                    lvBreakfast = (ListView) findViewById(R.id.lvFoodBreakfast);
                    FoodAdapter breakfastAdapter = new FoodAdapter(breakfasts);
                    lvBreakfast.setAdapter(breakfastAdapter);

                    lvLunch = (ListView) findViewById(R.id.lvFoodLunch);
                    FoodAdapter lunchAdapter = new FoodAdapter(lunches);
                    lvLunch.setAdapter(lunchAdapter);

                    lvDinner = (ListView) findViewById(R.id.lvFoodDinner);
                    FoodAdapter dinnerAdapter = new FoodAdapter(dinners);
                    lvDinner.setAdapter(dinnerAdapter);

                    lvSnacks = (ListView) findViewById(R.id.lvFoodSnacks);
                    FoodAdapter snackAdapter = new FoodAdapter(snacks);
                    lvSnacks.setAdapter(snackAdapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }


    @OnClick({R.id.tvFoodNext, R.id.tvFoodPrevious})
    public void changeDate(TextView textView) {
        int dayOfTheMonth = currentDate.get(Calendar.DAY_OF_MONTH);

        if (textView.getId() == R.id.tvFoodPrevious) {
            currentDate.set(Calendar.DAY_OF_MONTH, dayOfTheMonth - 1);
        }
        else {
            currentDate.set(Calendar.DAY_OF_MONTH, dayOfTheMonth + 1);
        }

        if (currentDate.getTime().toString().equals(today.getTime().toString())) {
            tvDate.setText(R.string.today);
        }
        else if (currentDate.getTime().toString().equals(tomorrow.getTime().toString())) {
            tvDate.setText(R.string.tomorrow);
        }
        else if (currentDate.getTime().toString().equals(yesterday.getTime().toString())) {
            tvDate.setText(R.string.yesterday);
        }
        else {
            tvDate.setText(sdf.format(currentDate.getTime()));
        }
        searchForFoods();
    }
}
