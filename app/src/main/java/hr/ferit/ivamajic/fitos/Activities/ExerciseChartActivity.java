package hr.ferit.ivamajic.fitos.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.ferit.ivamajic.fitos.Models.Exercise;
import hr.ferit.ivamajic.fitos.Models.ExerciseEntry;
import hr.ferit.ivamajic.fitos.R;

public class ExerciseChartActivity extends AppCompatActivity {

    @BindView(R.id.tvExerciseChartName) TextView tvName;

    ArrayList<ExerciseEntry> exerciseEntries = new ArrayList<>();

    String chosenExercise;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_chart);

        ButterKnife.bind(this);

        Bundle bundle = this.getIntent().getExtras();

        chosenExercise = bundle != null ? bundle.getString("exerciseName") : null;
        chosenExercise = chosenExercise != null ? chosenExercise.substring(1) : null;
        chosenExercise = chosenExercise != null ? chosenExercise.substring(0, chosenExercise.length() - 1) : null;
        Log.d("test3", chosenExercise);

        tvName.setText(chosenExercise + " progress");

        final GraphView graph = (GraphView) findViewById(R.id.graphExerciseChart);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        else {

            exerciseEntries = new ArrayList<>();
            DatabaseReference database = FirebaseDatabase.getInstance().getReference("exerciseEntries/" + user.getUid());
            database.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for (DataSnapshot exerciseEntrySnapshot : dataSnapshot.getChildren()) {
                        if (exerciseEntrySnapshot.child("exercise").child("name").getValue().toString().replaceAll("\\s+","").equalsIgnoreCase(chosenExercise.replaceAll("\\s+",""))) {
                            int series = Integer.parseInt(exerciseEntrySnapshot.child("exercise").child("seriesNo").getValue().toString());
                            int repetitions = Integer.parseInt(exerciseEntrySnapshot.child("exercise").child("repetitions").getValue().toString());
                            int weight = Integer.parseInt(exerciseEntrySnapshot.child("exercise").child("weight").getValue().toString());

                            String name = exerciseEntrySnapshot.child("exercise").child("name").getValue().toString();

                            Exercise exercise = new Exercise(name, repetitions, weight, series);
                            ExerciseEntry exerciseEntry = new ExerciseEntry(exerciseEntrySnapshot.getKey(), exerciseEntrySnapshot.child("date").getValue().toString(), exercise);
                            exerciseEntries.add(exerciseEntry);
                        }
                    }
                    ArrayList<DataPoint> dataPoints = new ArrayList<>();
                    Calendar limitDate = Calendar.getInstance();
                    limitDate.set(Calendar.DAY_OF_MONTH, 1);
                    int currentMonth = limitDate.get(Calendar.MONTH);
                    limitDate.set(Calendar.MONTH, currentMonth + 1);
                    int currentYear = limitDate.get(Calendar.YEAR);
                    limitDate.set(Calendar.YEAR, currentYear - 1);
                    SimpleDateFormat sdf = new SimpleDateFormat(getString(R.string.dateFormat));

                    for (ExerciseEntry entry : exerciseEntries) {
                        try {
                            Date date = sdf.parse(entry.getDate());
                            if (limitDate.getTime().before(date)) {
                                dataPoints.add(new DataPoint(date.getTime(), entry.getExercise().getWeight()));

                            }

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    Collections.sort(dataPoints, new Comparator<DataPoint>() {
                        @Override
                        public int compare(DataPoint dp1, DataPoint dp2) {
                           if (dp1.getX() == dp2.getX()) return 0;
                           return dp1.getX() < dp2.getX() ? -1 : 1;
                        }
                    });


                    LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dataPoints.toArray(new DataPoint[0]));
                    graph.addSeries(series);

                    graph.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(ExerciseChartActivity.this));
                    graph.getGridLabelRenderer().setNumHorizontalLabels(3);

                    graph.getViewport().setMinX(dataPoints.get(0).getX());
                    graph.getViewport().setMaxX(dataPoints.get(dataPoints.size() - 1).getX());
                    graph.getViewport().setXAxisBoundsManual(true);
                    graph.getGridLabelRenderer().setHumanRounding(false);
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.fitos, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this, ProfileActivity.class));
        return true;
    }
}
