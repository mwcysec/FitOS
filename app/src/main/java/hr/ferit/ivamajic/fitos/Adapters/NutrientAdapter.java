package hr.ferit.ivamajic.fitos.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hr.ferit.ivamajic.fitos.Models.API.FoodDetails.Nutrient;
import hr.ferit.ivamajic.fitos.R;

public class NutrientAdapter extends BaseAdapter {

    private ArrayList<Nutrient> mNutrients;

    public NutrientAdapter(ArrayList<Nutrient> nutrients) {
        mNutrients = nutrients;
    }

    @Override
    public int getCount() {
        return this.mNutrients.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mNutrients.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder nutrientViewHolder;

        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_nutrient, parent, false);
            nutrientViewHolder = new ViewHolder(convertView);
            convertView.setTag(nutrientViewHolder);
        }
        else {
            nutrientViewHolder = (ViewHolder) convertView.getTag();
        }

        Nutrient nutrient = this.mNutrients.get(position);
        nutrientViewHolder.tvNutrientName.setText(nutrient.getName());
        nutrientViewHolder.tvNutrientValue.setText(nutrient.getValue());
        nutrientViewHolder.tvNutrientUnit.setText(nutrient.getUnit());
        return convertView;
    }

    public static class ViewHolder {
        public TextView tvNutrientName;
        public TextView tvNutrientValue;
        public TextView tvNutrientUnit;

        public ViewHolder(View nutrientView) {
            tvNutrientName = (TextView) nutrientView.findViewById(R.id.tvNutrientItemName);
            tvNutrientValue = (TextView) nutrientView.findViewById(R.id.tvNutrientItemValue);
            tvNutrientUnit = (TextView) nutrientView.findViewById(R.id.tvNutrientItemUnit);
        }
    }
}
