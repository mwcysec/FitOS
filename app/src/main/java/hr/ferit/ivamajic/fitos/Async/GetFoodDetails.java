package hr.ferit.ivamajic.fitos.Async;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import hr.ferit.ivamajic.fitos.Activities.SearchFoodActivity;
import hr.ferit.ivamajic.fitos.Models.API.FoodDetails.FoodDetailsResponse;

public class GetFoodDetails extends AsyncTask<String, Double, FoodDetailsResponse> {

    private static final String API_URL = "https://api.nal.usda.gov/ndb/V2/reports?format=json&api_key=mmNrIPvpqU19VnR3CvheitEhOLRvmrlFN8KvSX8D&ndbno=";
    private SearchFoodActivity searchFoodActivity;
    private FoodDetailsResponse foodDetails;

    public GetFoodDetails(SearchFoodActivity activity) {
        searchFoodActivity = activity;
    }

    public void attach(SearchFoodActivity activity) {
        this.searchFoodActivity = activity;
    }

    public void detach() {
        this.searchFoodActivity = null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected FoodDetailsResponse doInBackground(String... id) {
        FoodDetailsResponse result = null;
        if (id == null) return result;

        for (int i=0; i<id.length; i++) {
            try {
                String searchID = id[i];
                result = retrieveSearchResults(searchID);
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private FoodDetailsResponse retrieveSearchResults(String ID) throws IOException {
        HttpsURLConnection connection = null;
        try {
            StringBuffer response = new StringBuffer();
            URL apiURL = new URL(API_URL + ID);
            connection = (HttpsURLConnection) apiURL.openConnection();
            if (connection.getResponseCode() == 200) {
                BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String inputLine;
                while((inputLine = input.readLine()) != null) {
                    response.append(inputLine);
                }
                foodDetails = new Gson().fromJson(String.valueOf(response), FoodDetailsResponse.class);
                input.close();
            }
            else {
                Log.d("fail", "fail");
            }
        }catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return foodDetails;
    }

    @Override
    protected void onPostExecute(FoodDetailsResponse details) {
        super.onPostExecute(details);
        if (this.searchFoodActivity != null) {
            this.searchFoodActivity.displayFoodDetails(details);
        }
    }
}
