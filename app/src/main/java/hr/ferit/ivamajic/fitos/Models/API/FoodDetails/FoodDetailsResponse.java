package hr.ferit.ivamajic.fitos.Models.API.FoodDetails;

import java.util.ArrayList;

public class FoodDetailsResponse {
    private ArrayList<InnerFoodResponse> foods;
    private int count, notfound, api;

    public int getApi() {
        return api;
    }

    public int getNotfound() {
        return notfound;
    }

    public int getCount() {
        return count;
    }

    public ArrayList<InnerFoodResponse> getFoods() {
        return foods;
    }
}
