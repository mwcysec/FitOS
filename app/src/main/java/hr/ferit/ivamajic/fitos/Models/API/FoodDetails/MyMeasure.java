package hr.ferit.ivamajic.fitos.Models.API.FoodDetails;

public class MyMeasure {
    private String label, eunit, value;
    private double eqv, qty;

    public String getLabel() {
        return label;
    }

    public String getEunit() {
        return eunit;
    }

    public String getValue() {
        return value;
    }

    public double getEqv() {
        return eqv;
    }

    public double getQty() {
        return qty;
    }
}
