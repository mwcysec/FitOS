package hr.ferit.ivamajic.fitos.Models;

public class Exercise {

    private String name;
    private int repetitions, weight, seriesNo;

    public Exercise(String name, int repetitions, int weight, int seriesNo) {
        this.name = name;
        this.repetitions = repetitions;
        this.weight = weight;
        this.seriesNo = seriesNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRepetitions() {
        return repetitions;
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getSeriesNo() {
        return seriesNo;
    }

    public void setSeriesNo(int seriesNo) {
        this.seriesNo = seriesNo;
    }
}
